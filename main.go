package main

import (
	"fmt"
	"os"

	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"gitlab.com/golang-studies/validate-password/api/docs"
	"gitlab.com/golang-studies/validate-password/internal/integration/http/controllers"
	"gitlab.com/golang-studies/validate-password/internal/integration/http/routes"
)

func HealthCheck(ctx *fiber.Ctx) error {
	return ctx.Status(fiber.StatusOK).JSON(&fiber.Map{
		"status": "success",
	})
}

// @title         Validate Password
// @version       1.0
// @description   Business Validate Password
// @contact.name  Jeisiel Ramos de Almeida Oliveira
// @contact.email jeisielramos@gmail.com
func main() {
	godotenv.Load()

	app := fiber.New()
	app.Get("/healthCheck", HealthCheck)

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8082"
	}

	swaggerBasePath := "/"

	docs.SwaggerInfo.BasePath = swaggerBasePath
	app.Get("/docs/*", swagger.New(swagger.Config{
		URL: fmt.Sprintf("%sdocs/doc.json", swaggerBasePath),
	}))

	ValidatePasswordControllers := controllers.NewValidatePasswordControllers()
	routes.SetupValidatePasswordRoutes(app, ValidatePasswordControllers)

	err := app.Listen(":" + httpPort)
	if err != nil {
		panic(err)
	}
}
