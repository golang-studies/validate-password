# Validate-password
    
An application to check password strength

## Getting started
The application need to have GoLang configured on your machine.
- [Go (lang)](https://go.dev/)

To run application, open `cmd|terminal` in the folder project and run code
```bash
    go run main.go
```