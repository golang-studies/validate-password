package entities

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/golang-studies/validate-password/internal/domain/enums"
)

func TestEntityRuleRequest(t *testing.T) {
	ruleTest := &Rule{
		Rule:  enums.RULE_MIN_SIZE,
		Value: 8,
	}

	jsonFile, err := os.Open("../../../test/resource/entities/rule-request.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var rulerq *Rule
	err = json.Unmarshal([]byte(byteValue), &rulerq)
	if err != nil {
		t.Errorf("failed json file to entity Rule | %s", err.Error())
	}

	if !reflect.DeepEqual(ruleTest, rulerq) {
		t.Errorf("Err json invalid")
	}
}
