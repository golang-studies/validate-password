package entities

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/golang-studies/validate-password/internal/domain/enums"
)

func TestEntityErrorResponse(t *testing.T) {
	errorResposeTest := NewErrorResponse(400, enums.ERROR_LOG_INVALID_REQUEST_BODY, "Unprocessable Entity")

	jsonFile, err := os.Open("../../../test/resource/entities/error-response.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var errorResponse *ErrorResponse
	err = json.Unmarshal([]byte(byteValue), &errorResponse)
	if err != nil {
		t.Errorf("failed json file to entity ErrorResponse | %s", err.Error())
	}

	if !reflect.DeepEqual(errorResposeTest, errorResponse) {
		t.Errorf("Err json invalid")
	}
}
