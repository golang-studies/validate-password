package entities

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
)

func TestEntityValidatePasswordResponse(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/entities/validate-password-rp.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var validadePasswordRp *ValidadePasswordRp
	err = json.Unmarshal([]byte(byteValue), &validadePasswordRp)

	if err != nil {
		t.Errorf("failed json file to entity ValidadePasswordRp | %s", err.Error())
	}
}
