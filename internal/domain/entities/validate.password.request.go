package entities

type ValidadePasswordRq struct {
	Password string  `json:"password"`
	Rules    []*Rule `json:"rules"`
}
