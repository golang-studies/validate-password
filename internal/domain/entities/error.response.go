package entities

import "gitlab.com/golang-studies/validate-password/internal/domain/enums"

type ErrorResponse struct {
	Status  int            `json:"status"`
	Error   enums.ErrorLog `json:"error"`
	Message string         `json:"message"`
}

func NewErrorResponse(status int, err enums.ErrorLog, message string) *ErrorResponse {
	return &ErrorResponse{
		status,
		err,
		message,
	}
}
