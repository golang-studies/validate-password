package entities

import "gitlab.com/golang-studies/validate-password/internal/domain/enums"

type ValidadePasswordRp struct {
	Verify bool          `json:"verify"`
	NoMath []*enums.Rule `json:"NoMath"`
}
