package entities

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/golang-studies/validate-password/internal/domain/enums"
)

// {
//     "password": "TesteSenhaForte!123&",
//     "rules": [
//         {"rule": "minSize","value": 8},
//         {"rule": "minSpecialChars","value": 2},
//         {"rule": "noRepeted","value": 0},
//         {"rule": "minDigit","value": 4}
//     ]
// }
func TestEntityValidatePasswordRequest(t *testing.T) {
	validadePasswordRqTest := &ValidadePasswordRq{
		Password: "TesteSenhaForte!123&",
		Rules: []*Rule{
			{Rule: enums.RULE_MIN_SIZE, Value: 8},
			{Rule: enums.RULE_MIN_SPECIAL_CHARS, Value: 2},
			{Rule: enums.RULE_NO_REPETED, Value: 0},
			{Rule: enums.RULE_MIN_MIN_DIGIT, Value: 4},
		},
	}

	jsonFile, err := os.Open("../../../test/resource/entities/validate-password-rq.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var validadePasswordRq *ValidadePasswordRq
	err = json.Unmarshal([]byte(byteValue), &validadePasswordRq)

	if err != nil {
		t.Errorf("failed json file to entity ValidadePasswordRq | %s", err.Error())
	}

	if !reflect.DeepEqual(validadePasswordRqTest, validadePasswordRq) {
		t.Errorf("Err json invalid")
	}
}
