package entities

import "gitlab.com/golang-studies/validate-password/internal/domain/enums"

type Rule struct {
	Rule  enums.Rule `json:"rule"`
	Value int        `json:"value"`
}
