package enums

type Rule string

const (
	RULE_MIN_SIZE          Rule = "minSize"
	RULE_MIN_SPECIAL_CHARS Rule = "minSpecialChars"
	RULE_NO_REPETED        Rule = "noRepeted"
	RULE_MIN_MIN_DIGIT     Rule = "minDigit"
)
