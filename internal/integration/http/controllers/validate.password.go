package controllers

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/golang-studies/validate-password/internal/application/services"
	"gitlab.com/golang-studies/validate-password/internal/domain/entities"
	"gitlab.com/golang-studies/validate-password/internal/domain/enums"
)

type ValidatePasswordControllers struct {
	service *services.ValidatePasswordService
}

func NewValidatePasswordControllers() *ValidatePasswordControllers {
	return &ValidatePasswordControllers{services.NewValidatePasswordService()}
}

// Verify godoc
// @Summary check the valid of password
// @Description Check the valid of password with the rules sending  in  request
// @Accept  json
// @Produce  json
// @Param body body entities.ValidadePasswordRq true "The text to finding smallest prime number"
// @Success 200 {object} entities.ValidadePasswordRp
// @Failure 403 {object} entities.ValidadePasswordRp
// @Failure 400 {object} entities.ErrorResponse
// @Router /verify [post]
func (controller *ValidatePasswordControllers) Verify(ctx *fiber.Ctx) error {
	validatePasswordRq := new(entities.ValidadePasswordRq)

	err := json.Unmarshal(ctx.Request().Body(), &validatePasswordRq)
	if err != nil {
		return err
	}
	if err != nil {
		return ctx.JSON(entities.NewErrorResponse(fiber.StatusBadRequest, enums.ERROR_LOG_INVALID_REQUEST_BODY, err.Error()))
	}

	validatePasswordRp := controller.service.Verify(validatePasswordRq)
	return ctx.JSON(validatePasswordRp)
}
