package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/golang-studies/validate-password/internal/integration/http/controllers"
)

const VALIDATE_PASSWORD_VERIFY = "/verify"

func SetupValidatePasswordRoutes(router fiber.Router, controller *controllers.ValidatePasswordControllers) {
	router.Post(VALIDATE_PASSWORD_VERIFY, controller.Verify)
}
